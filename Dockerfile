FROM openjdk:8

EXPOSE 8080

ADD /target/SpringBootApplicationWithDocker-0.0.1-SNAPSHOT.jar springbootapp.jar

ENTRYPOINT ["java","-jar","/springbootapp.jar"]
